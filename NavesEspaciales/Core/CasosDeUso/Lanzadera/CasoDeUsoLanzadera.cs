﻿using System;
using NavesEspaciales.Core.Entity;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.Core.Entity.Enums;
using NavesEspaciales.Utils;

namespace NavesEspaciales.Core.CasosDeUso.Lanzadera
{
    public static class CasoDeUsoLanzadera
    {
        public static NaveLanzadera CrearLanzadera()
        {
            Console.WriteLine("\n Creando Lanzadera: \n");
            var peso = UtilsConsole.LeerFloat("Ingrese el peso");
            var altura = UtilsConsole.LeerFloat("Ingrese la altura");
            var empuje = UtilsConsole.LeerInteger("Ingrese empuje");
            TipoCombustible tipoCombustible = UtilsConsole.LeerTipoCombustible();
            TipoCarga tipoCarga = UtilsConsole.LeerTipoCarga();
            var cantidad = UtilsConsole.LeerInteger("Ingrese cantidad de carga");


            NaveLanzadera nave = new NaveLanzadera()
            {
                Altura = altura,
                Peso = peso,
                Empuje = empuje,
                Combustible = new Combustible()
                {
                    Cantidad = cantidad,
                    TipoCombustible = tipoCombustible
                },
                Cantidad = cantidad,
                TipoCarga = tipoCarga
            };


            return nave;

        }
    }
}

