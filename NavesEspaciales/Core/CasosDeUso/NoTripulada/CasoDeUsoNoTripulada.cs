﻿using System;
using NavesEspaciales.Core.Entity;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.Core.Entity.Enums;
using NavesEspaciales.Utils;

namespace NavesEspaciales.Core.CasosDeUso.NoTripulada
{
    public static class CasoDeUsoNoTripulada
    {
        public static Nave CrearNoTripulada()
        {
            var opcion = UtilsConsole.LeerInteger("\nSeleccione Opción: \n1. No Orbitante \n2. Orbitante");
            if (opcion == 1)
            {
                return CrearNoTripuladaNoOrbitante();
            }
            else
            {
                return CrearNoTripuladaOrbitante();
            }
        }

        public static Nave CrearNoTripuladaOrbitante()
        {
            Console.WriteLine("\n Creando Nave No tripulada Orbitante: \n");
            var peso = UtilsConsole.LeerFloat("Ingrese el peso");
            var altura = UtilsConsole.LeerFloat("Ingrese la altura");
            var empuje = UtilsConsole.LeerInteger("Ingrese empuje");
            TipoCombustible tipoCombustible = UtilsConsole.LeerTipoCombustible();
            var cantidad = UtilsConsole.LeerInteger("Ingrese cantidad de carga");
            var distancia = UtilsConsole.LeerInteger("Ingrese distancia de órbita");
            var velocidad = UtilsConsole.LeerInteger("Ingrese velocidad de órbita");




            NoTripuladaOrbitante nave = new NoTripuladaOrbitante()
            {
                Altura = altura,
                Peso = peso,
                Empuje = empuje,
                Combustible = new Combustible()
                {
                    Cantidad = cantidad,
                    TipoCombustible = tipoCombustible
                },
                Distancia = distancia,
                Velocidad = velocidad
            };


            return nave;
        }

        public static Nave CrearNoTripuladaNoOrbitante()
        {
            Console.WriteLine("\n Creando Nave No tripulada No Orbitante: \n");
            var peso = UtilsConsole.LeerFloat("Ingrese el peso");
            var altura = UtilsConsole.LeerFloat("Ingrese la altura");
            var empuje = UtilsConsole.LeerInteger("Ingrese empuje");
            TipoCombustible tipoCombustible = UtilsConsole.LeerTipoCombustible();
            var cantidad = UtilsConsole.LeerInteger("Ingrese cantidad de carga");


            NoTripuladaNoOrbitante nave = new NoTripuladaNoOrbitante()
            {
                Altura = altura,
                Peso = peso,
                Empuje = empuje,
                Combustible = new Combustible()
                {
                    Cantidad = cantidad,
                    TipoCombustible = tipoCombustible
                }
            };


            return nave;
        }
    }
}

