﻿using System;
using NavesEspaciales.Core.CasosDeUso.Lanzadera;
using NavesEspaciales.Core.CasosDeUso.NoTripulada;
using NavesEspaciales.Core.CasosDeUso.Tripulada;
using NavesEspaciales.Core.Entity;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.View;

namespace NavesEspaciales.Core
{
    public static class CasosDeUsoGeneral
    {
       public static Nave CrearNave()
        {
            var opcion = GestorTextos.MenuCrearNave();
            Nave resultado = null;

            switch (opcion)
            {
                case 1:
                    resultado = CasoDeUsoLanzadera.CrearLanzadera();
                    break;
                case 2:
                    resultado = CasoDeUsoNoTripulada.CrearNoTripulada();
                    break;
                case 3:
                    CasoDeUsoTripulada.CrearTripulada();
                    break;
                default:
                    break;
            }
            return resultado;

        }
    }
}

