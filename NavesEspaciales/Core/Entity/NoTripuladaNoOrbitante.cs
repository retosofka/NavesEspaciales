﻿using System;
using NavesEspaciales.Core.Entity.Abstract;


namespace NavesEspaciales.Core.Entity
{
    public class NoTripuladaNoOrbitante : Nave
    {
        public NoTripuladaNoOrbitante()
        {
        }

        public override void Acelerar()
        {
            Console.WriteLine("Acelerando la nave No tripulada no orbitante");
        }

        public override void EnviarInformacion()
        {
            Console.WriteLine("Enviando Información desde la nave No tripulada no orbitante");

        }
    }
}

