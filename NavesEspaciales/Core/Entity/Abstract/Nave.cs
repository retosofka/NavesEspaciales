﻿
using System;

namespace NavesEspaciales.Core.Entity.Abstract

{
    public abstract class Nave
    {
        public float Peso { get; set; }
        public float Altura { get; set; }
        public Combustible Combustible { get; set; }
        public int Empuje { get; set; }

        public abstract void Acelerar();
        public abstract void EnviarInformacion();

        public void Imprimir()
        {
            Console.WriteLine($"Peso: {Peso}");
            Console.WriteLine($"Altura: {Altura}");
            Combustible.Imprimir();
            Console.WriteLine($"Empuje: {Empuje}");
        }

    }
}

