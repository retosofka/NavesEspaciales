﻿
using System;
using NavesEspaciales.Core.Entity.Enums;

namespace NavesEspaciales.Core.Entity.Abstract
{
    public class Combustible
    {
        public TipoCombustible TipoCombustible { get; set; }
        public float Cantidad { get; set; }

        public void Imprimir()
        {
            Console.WriteLine($"Tipo Combustible: {TipoCombustible}");
            Console.WriteLine($"Cantidad: {Cantidad}");
        }
    }
}