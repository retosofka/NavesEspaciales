﻿using System;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.Core.Entity.Enums;
using NavesEspaciales.Core.Entity.Interfaces;

namespace NavesEspaciales.Core.Entity
{
    public class NaveLanzadera : Nave, ICapacidad
    {

        public int Cantidad { get; set; }
        public TipoCarga TipoCarga { get; set; }

        public override void Acelerar()
        {
            Console.WriteLine("Acelerando la Lanzadera");
        }

        public override void EnviarInformacion()
        {
            Console.WriteLine("Enviando información desde Lanzadera");
        }
    }
}

