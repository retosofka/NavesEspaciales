﻿using System;
using NavesEspaciales.Core.Entity.Interfaces;
using NavesEspaciales.Core.Entity.Abstract;


namespace NavesEspaciales.Core.Entity
{
    public class NoTripuladaOrbitante : Nave, IOrbita
    {
        public NoTripuladaOrbitante()
        {
        }

        public float Velocidad { get; set; }
        public float Distancia { get; set; }

        public override void Acelerar()
        {
            throw new NotImplementedException();
        }

        public override void EnviarInformacion()
        {
            throw new NotImplementedException();
        }
    }
}

