﻿using System;
using NavesEspaciales.Core.Entity.Enums;

namespace NavesEspaciales.Core.Entity.Interfaces
{
    public interface ICapacidad
    {
        public int Cantidad { get; set; }
        public TipoCarga TipoCarga { get; set; }
    }
}

