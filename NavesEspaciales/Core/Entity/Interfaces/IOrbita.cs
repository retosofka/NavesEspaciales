﻿using System;
namespace NavesEspaciales.Core.Entity.Interfaces
{
    public interface IOrbita
    {
        public float Velocidad { get; set; }
        public float Distancia { get; set; }

    }
}

