﻿using System;
using NavesEspaciales.Core.Entity.Interfaces;
using NavesEspaciales.Core.Entity.Abstract;


namespace NavesEspaciales.Core.Entity
{
    public class TripuladaOrbitante : Nave, IOrbita
    {
        public TripuladaOrbitante()
        {
        }

        public float Velocidad { get; set; }
        public float Distancia { get; set; }

        public override void Acelerar()
        {
            Console.WriteLine("Acelerando desde Nave Tripulada Orbitante");
        }

        public override void EnviarInformacion()
        {
            Console.WriteLine("Enviando Información desde Nave Tripulada Orbitante");

        }
    }
}

