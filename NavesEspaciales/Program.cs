﻿using System;
using NavesEspaciales.Core;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.DataAccess;
using NavesEspaciales.Utils;
using NavesEspaciales.View;

namespace NavesEspaciales
{
    class Program
    {
        static void Main(string[] args)
        {
            IRepository repositorio = new LocalDatabase();

            while (true)
            {
                var opcion = GestorTextos.Menu();
                Nave nave = null;
                

                switch (opcion)
                {
                    case 1:
                        nave = CasosDeUsoGeneral.CrearNave();
                        repositorio.Guardar(nave);
                        break;
                    case 2:
                        var a = UtilsConsole.LeerTipoCombustible();
                        var r = repositorio.ObtenerPorTipoCombustible(a);
                        foreach (var item in r)
                        {
                            item.Imprimir();

                        }
                        break;
                    case 0:
                        Console.WriteLine("Hasta Luego");
                        break;

                    default:
                        break;
                }
            }
        }
    }


}
