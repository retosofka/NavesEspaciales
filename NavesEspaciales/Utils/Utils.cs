﻿using System;
using NavesEspaciales.Core.Entity.Enums;

namespace NavesEspaciales.Utils
{
    public static class UtilsConsole
    {
        public static float LeerFloat(string mensaje)
        {
            Console.WriteLine(mensaje);
            var result = false;
            float output = 0;

            while (!result)
            {
                result = float.TryParse(Console.ReadLine(), out output);
                if (!result)
                {
                    Console.WriteLine("Dato erroneo, intente nuevamente");
                }

            }

            return output;

        }

        public static int LeerInteger(string mensaje)
        {
            Console.WriteLine(mensaje);
            var result = false;
            int output = 0;

            while (!result)
            {
                result = int.TryParse(Console.ReadLine(), out output);
                if (!result)
                {
                    Console.WriteLine("Dato erroneo, intente nuevamente");
                }

            }

            return output;

        }

        public static TipoCombustible LeerTipoCombustible()
        {
            TipoCombustible tc;

            var tcv = LeerInteger("\nIngrese Tipo de combustible\n 1. SOLIDO \n2. LIQUIDO");

            if (tcv == 1)
            {
                tc = TipoCombustible.SOLIDO;
            }
            else
            {
                tc = TipoCombustible.LIQUIDO;

            }

            return tc;
        }

        public static TipoCarga LeerTipoCarga()
        {
            TipoCarga tc;

            var tcv = LeerInteger("\nIngrese Tipo de Carga\n 1. PERSONA \n2. CARGA UTIL");

            if (tcv == 1)
            {
                tc = TipoCarga.PERSONA;
            }
            else
            {
                tc = TipoCarga.CARGA_UTIL;
            }

            return tc;

        }
    }
}

