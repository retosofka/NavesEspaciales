﻿using System;
using System.Collections.Generic;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.Core.Entity.Enums;

namespace NavesEspaciales.DataAccess
{
    public class LocalDatabase : IRepository
    {
        public List<Nave> data = new List<Nave>();

        public LocalDatabase()
        {
        }

        public List<Nave> ObtenerPorTipoCombustible(TipoCombustible tc)
        {
            return data.FindAll(x => x.Combustible.TipoCombustible == tc);
        }

        public bool Guardar(Nave nave)
        {
            data.Add(nave);
            return true;
        }
    }
}

