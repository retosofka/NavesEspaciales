﻿using System.Collections.Generic;
using NavesEspaciales.Core.Entity.Abstract;
using NavesEspaciales.Core.Entity.Enums;

namespace NavesEspaciales.DataAccess
{
    public interface IRepository
    {
        public bool Guardar(Nave nave);
        public List<Nave> ObtenerPorTipoCombustible(TipoCombustible tc);
    }
}

