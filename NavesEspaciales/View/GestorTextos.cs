﻿using System;
namespace NavesEspaciales.View
{
    public static class GestorTextos
    {
        public static int Menu()
        {
            while (true)
            {
                Console.WriteLine("\nMenu: ");
                Console.WriteLine("");
                Console.WriteLine("1. Crear Nave ");
                Console.WriteLine("2. Buscar Por tipo combustible");
                Console.WriteLine("3. Busqueda Avanzada");
                Console.WriteLine("0. Salir");
                Console.WriteLine("");
                Console.WriteLine("Seleccione una opción: ");

                var seleccion = Console.ReadLine();

                if (!int.TryParse(seleccion, out int opcion))
                {
                    Console.WriteLine("Opción incorrecta, intente de nuevo");
                }else if (opcion >= 0 && opcion <= 3)
                {
                    return opcion;
                }
                else
                {
                    Console.WriteLine("Opción incorrecta, intente de nuevo");
                }
            }
        }

        public static int MenuCrearNave()
        {
            while (true)
            {
                Console.WriteLine("\nElija nave a crear: ");
                Console.WriteLine("");
                Console.WriteLine("1. Lanzadera");
                Console.WriteLine("2. No Tripulada");
                Console.WriteLine("3. Tripulada");
                Console.WriteLine("");
                Console.WriteLine("Seleccione una opción: ");

                var seleccion = Console.ReadLine();

                if (!int.TryParse(seleccion, out int opcion))
                {
                    Console.WriteLine("Opción incorrecta, intente de nuevo");
                }
                else if (opcion >= 1 && opcion <= 3)
                {
                    return opcion;
                }
                else
                {
                    Console.WriteLine("Opción incorrecta, intente de nuevo");
                }
            }
        }
    }
}

